SALES_TAX = .07

stock = []
shelf = []
cart = []
subtotal = 0.0
total = 0.0

def alphabetize(location):
    location.sort(key=lambda x: x[3])
    location.sort(key=lambda x: x[0])
    location.sort(key=lambda x: x[1])
    location.sort(key=lambda x: x[2])    
    
    
def display_stock():
    print("Here's what's in stock now:")
    for book in stock:
        print(f'{stock.index(book) + 1})', '"' + book[0] + '" -', book[1], book[2], '-', book[3])
        
    
def display_shelf():
    print("Here's what's on the shelf now:")
    for book in shelf:
        print(f'{shelf.index(book) + 1})', '"' + book[0] + '" -', book[1], book[2], '-', book[3])
        
        
def display_cart():
    global subtotal
    subtotal = 0.0
    print("Here's what's in your cart now:")
    for book in cart:
        print(f'{cart.index(book) + 1})', '"' + book[0] + '" -', book[1], book[2], '-', book[3])
        subtotal += book[3]
    print("Your current subtotal is $" + str(subtotal))
    
def add_to_stock(*args):
    try:
        title = input("Enter the book title:  ")
        author_first = input("Enter the author's first name:  ")
        author_last = input("Enter the author's last name:  ")
        price = float(input("Enter the price:  $"))
        new_stock_entry = [title, author_first, author_last, price]
        stock.append(new_stock_entry)
        alphabetize(stock)
        print("Book added to stock!")
        display_stock()
    except ValueError:
        print("Oops, the price of a book needs to be a number! Try again!")
        
        
def remove_from_stock(entry):
    del stock[entry - 1]
    print("Book removed from stock!")
    display_stock() 
    
    
def stock_to_shelf(entry):
    shelf.append(stock.pop(entry - 1))
    alphabetize(shelf) 
    print("Book moved to the shelf! It's for sale!")
    display_stock()
    
    
def shelf_to_stock(entry):
    stock.append(shelf.pop(entry - 1))             
    alphabetize(stock)
    print("Book removed from the shelf! It's no longer for sale.")
    display_shelf()
    
    
def shelf_to_cart(entry):
    cart.append(shelf.pop(entry - 1))             
    alphabetize(cart)
    print("Book added to your cart.")
    display_cart()
    
    
def cart_to_shelf(entry):
    shelf.append(cart.pop(entry - 1))
    alphabetize(cart)
    print("Book removed from your cart.")
    display_cart()
    
def customer_backout():
    for book in range(len(cart)):
        shelf.append(cart.pop(0))
    alphabetize(cart)
        

def finalize_purchase_interface():
    global subtotal
    global total
    global cart
    while True:
        total = subtotal * (1 + SALES_TAX)
        print(f"Including sales tax, your total is ${str(round(total, 2))}.")
        command = input("Would you like to complete the purchase? (YES/NO)  ").lower() 
        if command == "yes":
            cart = []
            subtotal = 0
            total = 0
            print("Thanks for your business! Have a nice day!")
            break
        elif command == "no":
            print("Take your time, and shop all you want!")
            break
        else:
            print("Sorry, that command didn't work. Try again!")
            continue
        
    
    
def employee_main_interface():
    while True:
        command = input("Would you like to edit the STOCK or SHELF, or QUIT?  ").lower()
        if command == "stock":
            employee_stock_interface()
        elif command == "shelf":
            employee_shelf_interface()
        elif command == "quit":
            break
        else:
            print("Sorry, that command didn't work. Try again!")
            continue

def employee_stock_interface():
    display_stock()
    while True:
        command = input("Would you like to ADD or REMOVE a book from stock, put a book on the shelf to SELL it, or go BACK?  ").lower()
        if command == "add":
            add_to_stock()
        elif command == "remove":
            try:
                remove_from_stock(int(input("Enter the item # to remove from stock:  ")))
            except ValueError:
                print("Sorry, that command didn't work. Try again!")
            except IndexError:
                print("Sorry, that command didn't work. Try again!")
        elif command == "sell":
            try:
                stock_to_shelf(int(input("Enter the item # to put it on the shelf:  ")))   
            except ValueError:
                print("Sorry, that command didn't work. Try again!")
            except IndexError:
                print("Sorry, that command didn't work. Try again!")
        elif command == "back":
            break
        else:
            print("Sorry, that command didn't work. Try again!")
            continue                  
        
        
def employee_shelf_interface():
    display_shelf()
    while True:
        command = input("Would you like to REMOVE a book from the shelf, or go BACK?  ").lower()
        if command == "remove":
            try:
                shelf_to_stock(int(input("Enter the item # to remove from the shelf:  ")))
            except ValueError:
                print("Sorry, that command didn't work. Try again!")
            except IndexError:
                print("Sorry, that command didn't work. Try again!")
        elif command == "back":
            break
        else:
            print("Sorry, that command didn't work. Try again!")
            continue
            
            
def customer_interface():
    while True:
        command = input("Would you like to ADD or REMOVE a book from your cart, go to CHECKOUT, or QUIT?  ").lower()
        if command == "add":
            display_shelf()
            try:
                shelf_to_cart(int(input("Enter an item # to put it in your cart:  ")))
            except ValueError:
                print("Sorry, that command didn't work. Try again!")
            except IndexError:
                print("Sorry, that command didn't work. Try again!")
        elif command == "remove":
            display_cart()
            try:
                cart_to_shelf(int(input("Enter an item # to remove it from you cart:  ")))
            except ValueError:
                print("Sorry, that command didn't work. Try again!")
            except IndexError:
                print("Sorry, that command didn't work. Try again!")
        elif command == "checkout":
            display_cart()
            finalize_purchase_interface()
        elif command == "quit":
            customer_backout()
            print("We'll put the items in your cart back on the shelf. Have a nice day!")
            break
        else:
            print("Sorry, that command didn't work. Try again!")
            continue
        
        
while True:
    command = input("Are you an EMPLOYEE or a CUSTOMER?  ").lower()
    if command == "employee":
        employee_main_interface()
    elif command == "customer":
        customer_interface()
    else:
        print("Sorry, that command didn't work. Try again!")
        continue